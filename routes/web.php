<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TableController;

Route::get('/', [HomeController::class, 'home']);
Route::get('/table', [TableController::class, 'table']);
Route::get('/data-tables', [TableController::class, 'datatables']);
