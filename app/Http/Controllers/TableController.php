<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function table()
    {
        return view('table', [
            'title' => 'Halaman Tabel'
        ]);
    }
    public function datatables()
    {
        return view('data-tables', [
            'title' => 'Halaman Data Tabel'
        ]);
    }
}
